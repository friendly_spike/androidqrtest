package com.project.customzxingtest;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class JiroGuideDialog extends Dialog {

    public JiroGuideDialog(Context context) {
        super(context);
        initUX();
    }

    public JiroGuideDialog(Context context, int delayTime) {
        super(context);
        initUX();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dismiss();
            }
        }, delayTime);
    }

    private void initUX(){
        requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(true);

        getWindow().setDimAmount(0.1f);

        setContentView(R.layout.dialog_jiro_guide);
        ((TextView)findViewById(R.id.btn_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
